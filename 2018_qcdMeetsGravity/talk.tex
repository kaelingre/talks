\documentclass[10pt]{beamer}

\usetheme{CambridgeUS}
\usecolortheme{rose}

\usepackage{psfrag,slashed,cancel,lscape,caption,array,graphicx,subcaption}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{mathrsfs}
\usepackage{multirow}
\usepackage{booktabs} %nice tables
\usepackage{xcolor}

\usepackage{stackengine}
\newcommand{\oset}[3][0.3ex]{\stackon[#1]{$#3$}{$\scriptstyle#2$}}
\newcommand{\uset}[3][0.3ex]{\stackunder[#1]{$#3$}{$\scriptstyle#2$}}

\DeclareFontFamily{OT1}{pzc}{}
\DeclareFontShape{OT1}{pzc}{m}{it}{<-> s * [1.350] pzcmi7t}{}
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}

\def\usegraphSmall#1#2{\includegraphics[scale=0.6,trim=0 #1 0 0]{graphs/#2.pdf}}

\setcounter{topnumber}{1}
\setcounter{tocdepth}{2}
 
\definecolor{myblue}{rgb}{0,0,0.7}
\def\GM#1{\textcolor{myblue}{{\bf\tt [GM: #1]}}}
\definecolor{mygreen}{rgb}{0,0.4,0}
\def\GK#1{\textcolor{mygreen}{{\bf\tt [GK: #1]}}}
\definecolor{myred}{rgb}{0.4,0,0}
\def\HJ#1{\textcolor{myred}{{\bf\tt [HJ: #1]}}}

\def\cN{\mathcal{N}}
\def\cO{\mathcal{O}}
\def\cD{\mathcal{D}}
\def\cQ{\mathcal{Q}}
\def\cV{\mathcal{V}}
\def\cH{\mathcal{H}}

\def\cT{\mathcal{T}}
\def\cM{\mathcal{M}}
\def\cA{\mathcal{A}}
\def\tf{\tilde{f}}
\def\k{\kappa}
\def\tk{\tilde{\kappa}}
\def\d{\mathrm{d}}

\def\nn{\nonumber}
\def\etat{\tilde{\eta}}
\def\etab{\bar{\eta}}
\def\mt{\widetilde{m}}
\def\lambdat{\tilde{\lambda}}
\newcommand{\widebar}{\overline}

\DeclareMathOperator{\ttr}{\rm tr}
\def\trm{\ttr_-}
\def\trp{\ttr_+}
\def\trfive{\ttr_5}
\def\eps{\epsilon}

\def\eqn#1{eq.~(\ref{#1})}
\def\Eqn#1{Equation~(\ref{#1})}
\def\eqns#1#2{eqs.~(\ref{#1}) and~(\ref{#2})}
\def\Eqns#1#2{Eqs.~(\ref{#1}) and~(\ref{#2})}

\def\sec#1{section~{\ref{#1}}}

\def\bra#1{\langle #1|}
\def\ket#1{|#1 \rangle}
\def\sqbra#1{[#1|}
\def\sqket#1{|#1]}
\def\braket#1{\langle #1 \rangle}

\def\kT{\hat{\kappa}}

\input{gTikZBeamer}
\usepackage{tikz}
\usetikzlibrary{mindmap,shadows}

\title{SQCD meets SUGRA at Two Loops}
\subtitle{\color{blue}work with H. Johansson \& G. Mogull [1706.09381]\\
and G. Mogull \& A. Ochirov [1811.09604]}
\author{Gregor Kälin}
\titlegraphic{\includegraphics[height=1.5cm]{img/UU_logo.png}}
\institute{11.12.2018}
\date{QCD meets Gravity}



\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{Motivation}
  \begin{center}
  \begin{tikzpicture}[
      mindmap,
      every node/.style={
        concept,
        execute at begin node=\hskip0pt,
        inner sep=0cm,
        minimum size=0
      },
      root concept/.append style={
        concept color=black, fill=white, line width=1ex, text=black,
        text width=2cm,
        inner sep=0cm,
        minimum size=0
      },
      text=white,
      level 1/.append style={level distance=3cm,sibling angle=90},
      level 2/.append style={level distance=2.5cm,sibling angle=45,minimum size=0,inner sep=0},
      level 3/.append style={level distance=2cm,sibling angle=45,minimum size=0,inner sep=0}
    ]
    \node [root concept] {Color-kinematics duality}
    child [grow=-150,concept color=red!70!black] { node {SUGRA}
      child[grow=-90] { node {UV}}
      child[grow=-135] { node {enhanced cancellations}}
      child[grow=180] { node {pure gravity}}
      child[grow=135] { node {axion \& dilaton}
        child[grow=0,concept color=blue!50!red] { node {matter ``ghosts''}}
      }
    }
    child [grow=-30,concept color=blue] { node {SQCD}
      child[grow=-90] { node {master numerators}}
      child[grow=-45] { node {SUSY decomposition}}
      child[grow=45] { node {unitarity cuts}
        child[grow=180,concept color=blue!50!red] { node {off-shell continuation}}
        child[grow=-75,concept color=blue!50!red] { node {locality?}}
      }
    };
  \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Plan}
  \begin{itemize}
  \item Factorizability Problem: Axion \& Dilaton
  \item Color-Kinematics Duality with Matter
  \item Non-Maximal Pure Supergravities
  \item (S)QCD Amplitude Constructions
  \item What's next?
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Problem: Axion \& Dilaton}
  Double copy of 4D pure YM on-shell states:
  \begin{equation*}
    A^\pm\otimes A^\pm= h^{++}\oplus h^{--}\oplus\underbracket{\phi\oplus a}_{\mathclap{\text{Axion \& Dilaton}}}
  \end{equation*}
  In general for $(\cN\leq2\text{ SYM})\times(\cM\leq2\text{ SYM})$:
  \begin{equation*}
    \cV_\cN \otimes \cV'_\cM \equiv \mathcal{H}_{\cN + \cM} \oplus X_{\cN + \cM} \oplus \widebar{X}_{\cN + \cM}
  \end{equation*}
  \begin{examples}
    \begin{itemize}
    \item $(\cN=4\text{ SYM})^2 = (\cN=8 \text{ SUGRA})$: 
      $\cV_4 \otimes \cV_4 \equiv \mathcal{H}_{8}$
    \item $(\cN=2\text{ SYM})^2 = (\cN=4 \text{ SUGRA} + \text{matter})$: 
      $\cV_2 \otimes \cV_2 \equiv \mathcal{H}_{4} \oplus 2\cV_{4}$
    \end{itemize}
  \end{examples}
  \emph{Can we prevent these additional states to propagate in loop amplitudes?}
\end{frame}

\begin{frame}
  \frametitle{Double copy of matter states\hfill {\small\color{black!60!green}[Johansson, Ochirov '14]}}
  \begin{block}{Solution}
    Use a double copy of matter multiplets ((half-)hyper, chiral, fermion)
    \begin{align*}
    \Phi_\cN \otimes \widebar{\Phi}'_\cM &\equiv X_{\cN + \cM} ,\\
    \widebar{\Phi}_\cN \otimes \Phi'_\cM &\equiv \widebar{X}_{\cN + \cM}
    \end{align*}
    to cancel out unwanted states (ghost statistics!).
  \end{block}
  \begin{examples}
    \begin{itemize}
    \item $(\cN=2\text{ SQCD})^2$
      \begin{equation*}
        \cV_2 \otimes \cV_2 \equiv \mathcal{H}_4 \oplus 2\cV_4 \qquad \qquad (\Phi_2\otimes\widebar{\Phi}_2) \oplus(\widebar{\Phi}_2\otimes\Phi_2) = 2\cV_4
      \end{equation*}
      \item $(\cN=2\text{ SQCD}) \times (\cN=0 \text{ QCD})$
      \begin{equation*}
        \cV_2 \otimes \cV_0 \equiv \mathcal{H}_2 \oplus \cV_2 \qquad \qquad (\Phi_2\otimes\widebar{\Psi}) \oplus(\widebar{\Phi}_2\otimes\Psi) = \cV_2
      \end{equation*}
    \end{itemize}
  \end{examples}
\end{frame}

\begin{frame}
  \frametitle{Double copy of on-shell states \hfill {\small\color{black!60!green}[Johansson, Ochirov '14]}}
  \centering
  \begin{tabular}{l c c c}
    \toprule
    $\text{(S)QCD}\otimes\text{(S)QCD}$ &  $\cV\otimes\cV$ & \multicolumn{1}{c@{~$\oplus$}}{$(\Phi\otimes\widebar{\Phi})$}  & $(\widebar{\Phi}\otimes\Phi)$ \\
    \midrule
    $\cN = 0+0$ & $h^{++}\oplus h^{--}\oplus\phi\oplus a$ & \multicolumn{1}{c@{~$\oplus$}}{$\phi$} & $a$ \\
    \midrule
    $\cN = 1+0$ & $\cH_{\cN=1}\oplus\Phi_{\cN=1}\oplus\widebar{\Phi}_{\cN=1}$ & \multicolumn{1}{c@{~$\oplus$}}{$\Phi_{\cN=1}$} & $\widebar{\Phi}_{\cN=1}$ \\
    \midrule
    $\cN = 2+ 0$ & $\cH_{\cN=2}\oplus\cV_{\cN=2}$ & \multicolumn{2}{c}{$\cV_{\cN=2}$} \\
    \midrule
    $\cN = 1+1$ & $\cH_{\cN=2}\oplus\Phi_{\cN=2}\oplus\widebar{\Phi}_{\cN=2}$ & \multicolumn{1}{c@{~$\oplus$}}{$\Phi_{\cN=2}$} & $\widebar{\Phi}_{\cN=2}$ \\
    \midrule
    $\cN = 2+1$ & $\cH_{\cN=3}\oplus\cV_{\cN=4}$ & \multicolumn{2}{c}{$\cV_{\cN=4}$}\\
    \midrule
    $\cN = 2+2$ & $\cH_{\cN=4}\oplus2\cV_{\cN=4}$ & \multicolumn{2}{c}{$2\cV_{\cN=4}$}\\
    \bottomrule
  \end{tabular}
  \begin{block}{}
    All pure $\cN=0,1,2,3,4$ supergravities can be constructed from $\cN=0,1,2$ SQCD.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Color-kinematics duality with matter \hfill {\small\color{black!60!green}[Johansson, Ochirov '14]}}
  General $L$-loop gauge theory amplitude:
  \begin{equation*}
    \cA_m^{(L)}=i^{L-1}g^{m+2L-2}\sum_{\text{cubic graphs }\Gamma_i}\int\!\frac{\d^{LD}\ell}{(2\pi)^{LD}}\frac{1}{S_i} \frac{n_i c_i}{D_i}
  \end{equation*}
  We seek a duality between color and kinematics:
  \begin{align*}
    n_i-n_j = n_k \Longleftrightarrow c_i-c_j = c_k, \qquad n_i = -n_j \Longleftrightarrow c_i = -c_j
  \end{align*}
  Expressed in a diagramatic language:
  %\tikzset{external/force remake}%
  \begin{align*}
    \gTreeS[all=gluon]{} &= \gTreeT[all=gluon]{} - \gTreeU[all=gluon]{} & & \text{(Jacobi identity)}\\
    \gTreeS[all=gluon,eC=quark,eD=aquark]{} &= \gTreeT[all=gluon,eC=quark,eD=quark,iA=aquark]{}
    - \gTreeU[all=gluon,eC=quark,eD=aquark,iA=aquark]{} & & \text{(commutation relation)}\\
    n\left(\gTreeS[eA=quark,eB=aquark,eC=quark,eD=aquark,iA=gluon]{}\right) &\overset{?}{=}
    n\left(\gTreeT[eA=quark,eB=aquark,eC=quark,eD=aquark,iA=gluon]{}\right) & & \text{(two-term identity)}
\end{align*}
\end{frame}

%% \begin{frame}
%%   \frametitle{Double copy of states: 6D}
%%   \centering
%%   \begin{tabular}{l c c c}
%%     \toprule
%%     6D $\text{SYM}\otimes\text{SYM}$ &  $\cV\otimes\cV$ & \multicolumn{1}{c@{~$\oplus$}}{$(\Phi\otimes\widebar{\Phi})$}  & $(\widebar{\Phi}\otimes\Phi)$ \\
%%     \midrule
%%     \multirow{2}{*}{$\cN = (0,0)\otimes(0,0)$} & \multirow{2}{*}{${h^{ab}}_{\dot{a}\dot{b}}\oplus B^{ab}\oplus B_{\dot{a}\dot{b}} \oplus \phi$} & \multicolumn{1}{c@{~$\oplus$}}{${A^a}_{\dot{a}}$} & ${A^a}_{\dot{a}}$ \\ & & \multicolumn{1}{c@{~$\oplus$}}{$B^{ab}\oplus\phi$} & $B_{\dot{a}\dot{b}}\oplus\phi$ \\
%%     \midrule
%%     \multirow{2}{*}{$\cN = (1,0)\otimes(0,0)$} & \multirow{2}{*}{$\cH_{\cN = (1,0)}\oplus\cT_{\cN=(1,0)}$} & $\cT_{\cN=(1,0)}$ & -- \\ & & \multicolumn{1}{c@{~$\oplus$}}{$\cV_{\cN=(1,0)}$} & $\cV_{\cN=(1,0)}$ \\
%%     \midrule
%%     $\cN = (1,1)\otimes(0,0)$ & $\cH_{\cN=(1,1)}$ & \multicolumn{2}{c}{--} \\
%%     \midrule
%%     $\cN = (1,0)\otimes(0,1)$ & $\cH_{\cN=(1,1)}$ & \multicolumn{1}{c@{~$\oplus$}}{$\cV_{\cN=(1,1)}$} & $\cV_{\cN=(1,1)}$ \\
%%     \midrule
%%     $\cN = (1,0)\otimes(1,0)$ & $\cH_{\cN=(2,0)}\oplus\cT_{\cN=(2,0)}$ & $\cT_{\cN=(2,0)}$ & -- \\
%%     \midrule
%%     $\cN = (1,1)\otimes(1,0)$ & $\cH_{\cN=(2,1)}$ & \multicolumn{2}{c}{--}\\
%%     \midrule
%%     $\cN = (1,1)\otimes(1,1)$ & $\cH_{\cN=(2,2)}$ & \multicolumn{2}{c}{--}\\
%%     \bottomrule
%%   \end{tabular}  
%% \end{frame}

\begin{frame}
  \frametitle{Double copy: Explicit construction}
  Cancel out unwanted states to obtain a pure SUGRA amplitude:
  \begin{equation*}
    \cM_m^{(L)}=
  i^{L-1}\left(\frac{\kappa}{2}\right)^{m+2L-2}\sum_{\text{cubic graphs }\Gamma_i}
  \int\!\frac{\d^{LD}\ell}{(2\pi)^{LD}}\frac{(N_f)^{\lvert i\rvert}}{S_i}\frac{n_i\bar{n}'_i}{D_i}
  \end{equation*}
  Ghosts statistics: $N_f=-1$; $N_f>-1$ supergravity theories with any number of matter multiplets.\\
  In practice:
  %\tikzset{external/force remake}%
  \begin{align*}
    %\tikzset{external/force remake}%
    &N\left(\gBoxBox[scale=0.7,all=gluon]{
      \node[] at (0.9,1.2) {\scriptsize$\cN=4$};
    }\right)=
    %\tikzset{external/force remake}%
    \left|n\left(\gBoxBox[scale=0.7,all=gluon]{
      \node[] at (0.9,1.2) {\scriptsize$\cN=2$};
    }\right)\right|^2\\
    &+2 N_f\left(
    %\tikzset{external/force remake}%
    \left|n\left(\gBoxBox[scale=0.7,all=gluon,iA=quark,iB=quark,iC=quark,iD=quark,iE=quark,iF=quark]{
      \node[] at (0.9,1.2) {\scriptsize$\cN=2$};
    }\right)\right|^2
    %\tikzset{external/force remake}%
    +\left|n\left(\gBoxBox[scale=0.7,all=gluon,iA=quark,iB=quark,iG=quark,iF=quark]{
      \node[] at (0.9,1.2) {\scriptsize$\cN=2$};
    }\right)\right|^2
    %\tikzset{external/force remake}%
    +\left|n\left(\gBoxBox[scale=0.7,all=gluon,iC=quark,iD=quark,iE=quark,iG=aquark]{
      \node[] at (0.9,1.2) {\scriptsize$\cN=2$};
    }\right)\right|^2
    \right)
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Status report: Two-loop}
  We have obtained:
  \begin{itemize}
  \item Color-dual $\cN=2$ SQCD numerators for all possible configurations of external states (vectors + hypers).
  \item Color-dual $\cN=1$ SQCD numerators for external vector states.
  \item Non-color-dual $\cN=0$ numerators for external gluon states using Feynman rules.
  \end{itemize}
  \begin{block}{}
    Using these numerators we obtained $\cN=(2+2)$, $\cN=(2+1)$, $\cN=(1+1)$, $\cN=(2+0)$ and $\cN=(1+0)$ pure supergravity numerators at two loops. This allows us to study their UV behavior in $D\leq 6$ (work in progress).
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{UV divergences and counterterms}
  \begin{itemize}
  \item In $D=4$ the $R^3$ counterterm is forbidden by supersymmetry.
  \item In $D=5$ we reproduced the enhanced cancellation for $\cN=4$ supergravity using a double copy of the form $(\cN=2 \text{ SQCD})\times(\cN=2 \text{ SQCD})$.
  \item Enhanced cancellations also for $\cN=2$ supergravity? We are working on it.
  \item In $D=4$ the first non-trivial UV behaviour starts at three loops:
    \begin{itemize}
    \item Ansatz construction: possible, but hard.
      \item Better: \\Reduce the number of master numerators (size of the Ansatz) using various constraints. \\Use off-shell lift of cuts to significantly simplify the Ansatz further or even guess the whole solution.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{$\cN=2+2$ supergravity: Enhanced cancellations}
  \begin{itemize}
  \item Already previously obtained via $(\cN=4)\times(\cN=0)$ at two loops {\small\color{black!60!green}[Boucher-Veronneau, Dixon '11]}\\
  \item Reproduces enhanced cancellations in $D=5-2\eps$ {\small\color{black!60!green}[Bern, Davies, Dennen, Huang '12]}
    \end{itemize}
  \footnotesize
  \begin{align*}
    &\mathcal{I}^{5-2\eps} \bigg[N^{[\cN=4\text{ SG}]}\bigg(\usegraphSmall{9}{nBoxBoxs}\bigg)\bigg]=\\
    &\,\,\frac{1}{(4\pi)^5}\left(\frac{(2-D_s)\pi}{70\eps}(\kappa_{12}^2+\kappa_{34}^2)-
    \frac{(29D_s-142)\pi}{210\eps}(\kappa_{13}^2+\kappa_{14}^2+\kappa_{23}^2+\kappa_{24}^2)\right)+\cO(\eps^0)\\
    &\mathcal{I}^{5-2\eps}\bigg[N^{[\cN=4\text{ SG}]}\bigg(\!\usegraphSmall{9}{nPentaTris}\bigg)\bigg]=\frac{1}{(4\pi)^5}\frac{(D_s-2)\pi}{5\eps}\sum_{i<j}\kappa_{ij}^2+\cO(\eps^0)\\
    &\mathcal{I}^{5-2\eps}\bigg[N^{[\cN=4\text{ SG}]}\bigg(\!\!\usegraphSmall{9}{nBoxBoxNPs}\bigg)\bigg]=\\
    &\,\,\frac{1}{(4\pi)^5}\left(\frac{(18-23D_s)\pi}{105\eps}(\kappa_{12}^2+\kappa_{34}^2)-
    \frac{2(4+5D_s)\pi}{105\eps}(\kappa_{13}^2+\kappa_{14}^2+\kappa_{23}^2+\kappa_{24}^2)\right)+\cO(\eps^0)\\
    %&\mathcal{I}^{5-2\eps}\bigg[N^{[\cN=4\text{ SG}]}\bigg(\usegraphSmall{9}{nTriTris}\bigg)\bigg]=\cO(\eps^0)\\
    &\mathcal{I}^{5-2\eps}\bigg[N^{[\cN=4\text{ SG}]}\bigg(\!\usegraphSmall{9}{nHexBubs}\bigg)\bigg]= -\frac{1}{(4\pi)^5}\frac{(D_s-2)\pi}{6\eps}\sum_{i<j}\kappa_{ij}^2+\cO(\eps^0).
  \end{align*}
  \normalsize
\end{frame}

\begin{frame}
  \frametitle{$\cN=2$ SQCD: How to}
  \begin{enumerate}
  \item Solve Jacobi identities to obtain master numerators.
  \item Solve two-term identities to reduce the set of master numerators:
    \begin{equation*}
      n\left(\gTreeS[eA=quark,eB=aquark,eC=quark,eD=aquark,iA=gluon]{}\right) \overset{?}{=}
      n\left(\gTreeT[eA=quark,eB=aquark,eC=quark,eD=aquark,iA=gluon]{}\right)
    \end{equation*}
  \item Implement supersymmetric decomposition identities to reduce the set of master numerators:
      %\tikzset{external/force remake}
      \begin{align*}
      n^{[\cN=4]}\left(\gBoxBox[scale=0.7,all=gluon]{}\right) = &n^{[\cN=2]}\left(\gBoxBox[scale=0.7,all=gluon]{}\right) + 2n^{[\cN=2]}\left(\gBoxBox[scale=0.7,all=gluon,iC=quark,iD=quark,iE=quark,iG=aquark]{}\right)\\
      &+ 2n^{[\cN=2]}\left(\gBoxBox[scale=0.7,all=gluon,iA=quark,iB=quark,iG=quark,iF=quark]{}\right) + 2n^{[\cN=2]}\left(\gBoxBox[scale=0.7,all=gluon,iA=quark,iB=quark,iC=quark,iD=quark,iE=quark,iF=quark]{}\right)
    \end{align*}
  \item Make Ans\"atze and implement constraints and further desirable properties
    \begin{itemize}
    \item {\only<2->{\color{red!80!black}}Unitarity cuts}
    \item Manifest CPT
    \item Matter reversal: $\Phi$ and $\widebar{\Phi}$ have the same particle content.
    \end{itemize}
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{$\cN=2$ SQCD: Iterated two-particle cuts (4D)}
  Idea 1: Systematize supersum computations {\small\color{black!60!green}[Bern, Carrasco, Johansson, Roiban '12]}\\
  Idea 2: Iteratively glue four-point blobs together:
  \begin{equation*}
    %\tikzset{external/force remake}
    \begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize]
      \fill[blob] (-0.5,0) ellipse (0.2 and 0.3);
      \fill[blob] (0.5,0) ellipse (0.3 and 0.4);
      \filldraw[fill=white,draw=red!40!black] (0.55,0.15) circle (0.1);
      \filldraw[fill=white,draw=red!40!black] (0.45,-0.1) circle (0.1);
      \draw[line] ($(-0.5,0) + (135:0.23)$) -- ($(-0.5,0) + (135:0.5)$);
      \draw[line] ($(-0.5,0) + (-135:0.23)$) -- ($(-0.5,0) + (-135:0.5)$);
      \draw[line] ($(-0.5,0) + (30:0.23)$) -- ($(0.5,0) + (157:0.3)$);
      \draw[line] ($(-0.5,0) + (-30:0.23)$) -- ($(0.5,0) + (-157:0.3)$);
      \draw[line] ($(0.5,0) + (45:0.35)$) -- ($(0.5,0) + (45:0.7)$);
      \draw[line] ($(0.5,0) + (-45:0.35)$) -- ($(0.5,0) + (-45:0.7)$);
    \end{tikzpicture}
  \end{equation*}
  \begin{center}
  \small
  \begin{tabular}{c |c | c}
    General rules & Internal rules & External rules\\
    \midrule
    %\tikzset{external/force remake}
    $\begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize]
      \fill[blob] (0,0) circle (0.2);
      \draw[line] (45:0.2) -- (45:0.5) node[right] {$a$};
      \draw[line] (-45:0.2) -- (-45:0.5) node[right] {$b$};
      \draw[line] (-135:0.2) -- (-135:0.5) node[left] {$c$};
      \draw[line] (135:0.2) -- (135:0.5) node[left] {$d$};
    \end{tikzpicture}
    \rightarrow -\frac{i}{s_{ab}s_{bc}}$
    &
    %\tikzset{external/force remake}
    $\begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize]
      \fill[blob] (0,0) circle (0.2);
      \draw[gluon] (45:0.2) -- (45:0.5) node[right] {$a^-$};
      \draw[gluon] (-45:0.2) -- (-45:0.5) node[right] {$b^-$};
      \draw[gluon] (-135:0.2) -- (-135:0.5) node[left] {$c^+$};
      \draw[gluon] (135:0.2) -- (135:0.5) node[left] {$d^+$};
    \end{tikzpicture}
    \rightarrow \braket{ab}[cd]$
    &
    %\tikzset{external/force remake}
    $\begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize]
      \draw[dotted] (0,0) circle (0.2);
      \draw[gluon] (45:0.2) -- (45:0.5) node[right] {$q^-$};
      \draw[gluon] (-45:0.2) -- (-45:0.5) node[right] {$r^-$};
      \draw[gluon] (-135:0.2) -- (-135:0.5) node[left] {$s^+$};
      \draw[gluon] (135:0.2) -- (135:0.5) node[left] {$t^+$};
    \end{tikzpicture}
    \!\rightarrow [qr]\braket{st}\kT_{(qr)(qr)}$
    \\
    %\tikzset{external/force remake}
    $\begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize]
      \fill[pattern color=red!40!black,pattern=north west lines] (-0.5,0) -- (-0.5,-0.25) arc (-90:90:0.25) -- cycle;
      \draw[red!40!black] (-0.5,-0.25) arc (-90:90:0.25);
      \fill[pattern color=red!40!black,pattern=north west lines] (0.5,0) -- (0.5,0.25) arc (90:270:0.25) -- cycle;
      \draw[red!40!black] (0.5,0.25) arc (90:270:0.25);
      \draw[line] ($(-0.5,0) + (30:0.25)$) -- node[above] {$\uset{\rightarrow}{l_1}$} ($(0.5,0) + (150:0.25)$);
      \draw[line] ($(-0.5,0) + (-30:0.25)$) -- node[below] {$\oset{\rightarrow}{l_2}$} ($(0.5,0) + (-150:0.25)$);
    \end{tikzpicture}
    \rightarrow s_{l_1l_2}$
    &
    %\tikzset{external/force remake}
    $\begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize]
      \fill[blob] (0,0) circle (0.2);
      \draw[gluon] (45:0.2) -- (45:0.5) node[right] {$a^-$};
      \draw[gluon] (-45:0.2) -- (-45:0.5) node[right] {$b^+$};
      \draw[quark] (-135:0.2) -- (-135:0.5) node[left] {$c$};
      \draw[aquark] (135:0.2) -- (135:0.5) node[left] {$d$};
    \end{tikzpicture}
    \rightarrow \langle a|c|b]$
      &
      %\tikzset{external/force remake}
      $\begin{tikzpicture}
        [line width=1pt,
          baseline={([yshift=-0.5ex]current bounding box.center)},
          font=\scriptsize]
        \draw[dotted] (0,0) circle (0.2);
        \draw[gluon] (45:0.2) -- (45:0.5) node[right] {$q^-$};
        \draw[gluon] (-45:0.2) -- (-45:0.5) node[right] {$r^+$};
        \draw[quark] (-135:0.2) -- (-135:0.5) node[left] {$s$};
        \draw[aquark] (135:0.2) -- (135:0.5) node[left] {$t$};
      \end{tikzpicture}
      \!\rightarrow  [q|s|r\rangle\kT_{(qs)(qt)}$
        \\
        &
        %\tikzset{external/force remake}
        $\begin{tikzpicture}
          [line width=1pt,
            baseline={([yshift=-0.5ex]current bounding box.center)},
            font=\scriptsize]
          \fill[blob] (0,0) circle (0.2);
          \draw[quark] (45:0.2) -- (45:0.5) node[right] {$a$};
          \draw[aquark] (-45:0.2) -- (-45:0.5) node[right] {$b$};
          \draw[quark] (-135:0.2) -- (-135:0.5) node[left] {$c$};
          \draw[aquark] (135:0.2) -- (135:0.5) node[left] {$d$};
        \end{tikzpicture}
        \rightarrow s_{ac} = s_{bd}$
        &
        %\tikzset{external/force remake}
        $\begin{tikzpicture}
          [line width=1pt,
            baseline={([yshift=-0.5ex]current bounding box.center)},
            font=\scriptsize]
          \draw[dotted] (0,0) circle (0.2);
          \draw[quark] (45:0.2) -- (45:0.5) node[right] {$q$};
          \draw[aquark] (-45:0.2) -- (-45:0.5) node[right] {$r$};
          \draw[quark] (-135:0.2) -- (-135:0.5) node[left] {$s$};
          \draw[aquark] (135:0.2) -- (135:0.5) node[left] {$t$};
        \end{tikzpicture}~\,
        \!\rightarrow s_{rt}\kT_{(qs)(rt)}$
  \end{tabular}
  \end{center}
  \begin{equation*}
    \kT_{(qr)(st)} \equiv \frac{s t A^{\text{tree}}}{s_{qr}s_{st}}
  \end{equation*}
\end{frame}

\begin{frame}
  \frametitle{Cut properties}
  \begin{itemize}
  \item Factorized rules for each blob
  \item Poles are handled transparently (locality!).
    \begin{itemize}
    \item Physical poles from each tree-level blob
    \item Spinor-helicity remnants in $\kT$      
    \end{itemize}
  \item Regulated matter legs in the IR, e.g.
    \begin{equation*}
      %\tikzset{external/force remake}
      \begin{tikzpicture}
        [line width=1pt,
          baseline={([yshift=-0.5ex]current bounding box.center)},
          font=\scriptsize]
        \fill[blob] (0,0) circle (0.2);
        \draw[gluon] (45:0.2) -- (45:0.5) node[right] {$a^-$};
        \draw[gluon] (-45:0.2) -- (-45:0.5) node[right] {$b^+$};
        \draw[quark] (-135:0.2) -- (-135:0.5) node[left] {};
        \draw[aquark] (135:0.2) -- (135:0.5) node[left] {};
        \draw[line width=0.45pt,-to] (160:0.35) -- node[left] {$\ell$} (200:0.35);
      \end{tikzpicture}
      \rightarrow \langle a|\ell|b]
    \end{equation*}
  \item Natural two-term identity
  \item Numerators form dirac traces
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Off-shell continuation ($\sim$ the rung rule)}
  Idea: motivated by the two-particle cut in $\cN=4$ numerators are constructed via a \emph{rung rule} {\small\color{black!60!green}[Bern, Rozowsky, Yan '97; Bern, Dixon, Dunbar, Perelstein, Rozowsky '98]}
  \begin{equation*}
  %\tikzset{external/force remake}%
  \begin{tikzpicture}
    [line width=1pt,
    baseline={([yshift=-0.5ex]current bounding box.center)},
    font=\scriptsize]
    \draw[dotted] (-0.3,0.4) -- (0,0.4);
    \draw[line] (0,0.4) -- node[above] {$\uset{\rightarrow}{\ell_1}$} (0.5,0.4);
    \draw[dotted] (0.5,0.4) -- (0.9,0.4);
    \draw[dotted] (-0.3,0) -- (0,0);
    \draw[line] (0,0) -- node[below] {$\oset{\rightarrow}{\ell_2}$} (0.5,0);
    \draw[dotted] (0.5,0) -- (0.9,0);
  \end{tikzpicture}
  \rightarrow -i(\ell_1+\ell_2)^2\times
  %\tikzset{external/force remake}%
  \begin{tikzpicture}
    [line width=1pt,
    baseline={([yshift=-0.5ex]current bounding box.center)},
    font=\scriptsize]
    \draw[dotted] (-0.3,0.4) -- (0,0.4);
    \draw[line] (0.0,0.4) node[above] {$\uset{\rightarrow}{\ell_1}$} -- (0.5,0.4);
    \draw[dotted] (0.5,0.4) -- (0.9,0.4);
    \draw[dotted] (-0.3,0) -- (0,0);
    \draw[line] (0,0) node[below] {$\oset{\rightarrow}{\ell_2}$} -- (0.5,0);
    \draw[dotted] (0.5,0) -- (0.9,0);
    \draw[line] (0.25,0.4) -- (0.25,0);
  \end{tikzpicture}
  \end{equation*}
  Can we also lift $\cN=2$ cuts off-shell? E.g.
  %\tikzset{external/force remake}
  \begin{equation*}
    %\tikzset{external/force remake}
    \cBoxA[all=gluon,eLA=$1^-$,eLB=$2$,eLC=$3$,eLD=$4^+$,iLB=$\oset{\rightarrow}{l_1}$,
      iLA=$\uset{\leftarrow}{l_2}$,
      eB=quark,eC=aquark,iB=quark]{}=
    \frac{\trp(4l_1l_2l_112)}{s\,s_{4l_1}s_{2l_2}}\kT_{(12)(13)}=
    -\frac{\trp(4l_112)}{s_{4l_1}s_{2l_2}}\kT_{(12)(13)}
  \end{equation*}
  \begin{equation*}
    \Longrightarrow n\left(\gBox[all=gluon,yshift=-1,eLA=$1$,eLB=$2$,eLC=$3$,eLD=$4$,iA=quark,iD=quark,iC=quark,
eB=quark,eC=aquark,iLD=$\oset{\rightarrow}{\ell}$]{}\right)=
\trp(4\ell12)\kT_{(12)(13)}+\trm(4\ell12)\kT_{(24)(34)}
  \end{equation*}
\end{frame}

\begin{frame}
  \frametitle{When does it work?}
  \begin{itemize}
  \item Mondrian-type diagrams can in many cases directly be read off.
  \item Otherwise (if cuts do not agree): significantly reduced ansatz.
  \item Choose master numerators that we can get from the cuts like this.
  \item Jacobi relations, two-term identities, SUSY decomposition identities and further constraints give all other diagrams.
  \item The off-shell continuation clashes with the color-kinematics duality for some numerators:
    \begin{itemize}
      \item Use the construction to find local (S)QCD numerators.
    \end{itemize}
  \item Missing $\mu$ terms are in general much simpler: use 6D spinor helicity formalism {\small\color{black!60!green}[Cheung, O'Connell '09]}  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{$\cN=2$ SCQD: Example results and their properties I}
  \vspace{-0.8cm}
  \begin{align*}
    %\tikzset{external/force remake}
    \cBoxBoxB[all=gluon,eLA=$3^-$,eLB=$4^+$,eLC=$1^-$,eLD=$2^+$,iA=quark,iB=aquark,
      iC=aquark,iLD=$\!\uparrow\!\!l_3$,iLF=$\downarrow\!l_1$,iLA=$l_2\!\downarrow$]{}
    &\begin{aligned}[t]
      &=-\frac{\langle3|l_2|4]}{(l_2+p_4)^2(l_2-p_3)^2}\times s\times\frac{\langle1|l_1|2]}{s\,l_1^2}\times
        [13]\braket{24}\kT_{13}\\
        &=\frac{\trm(1l_124l_23)}{l_1^2(l_2+p_4)^2(l_2-p_3)^2}\kT_{13}\,,
     \end{aligned}\\
    %\tikzset{external/force remake}
    \Rightarrow n\!\left(\gBoxBox[all=gluon,scale=.8,eLA=$1$,eLB=$2$,eLC=$3$,eLD=$4$,iA=aquark,
      iB=aquark,iF=aquark,iE=aquark,iD=aquark,iC=aquark,iLA=$\downarrow\!\ell_1$,
      iLD=$\ell_2\!\downarrow$]{}\right)
    &=\begin{aligned}[t]
      &\kT_{13}\trm(1\ell_124\ell_23)+\kT_{14}\trm(1\ell_123\ell_24)\\
      &+\kT_{23}\trp(1\ell_123\ell_24)+\kT_{24}\trp(1\ell_124\ell_23)
      \end{aligned}\\
    %\tikzset{external/force remake}
    \cBoxBoxA[all=gluon,eA=quark,eB=aquark,eLA=$1$,eLB=$2$,eLC=$3$,
      eLD=$4$,iA=quark,iB=quark,iC=quark,iD=quark,iLE=$\downarrow\!l_1$,
      iLF=$\!\uparrow\!\!l_3$,iLG=$l_2\!\downarrow$]{}\!
    &=\!\!\begin{aligned}[t] &
      -\frac{\trm(4l_231)(s+l_3^2)(s+l_1^2)}{sl_1^2l_2^2l_3^2}\kT_{(41)(42)} \\ &
      -\frac{\trp(4l_231)(s+l_3^2)(s+l_1^2)}{sl_1^2l_2^2l_3^2}\kT_{(31)(32)}\,,
    \end{aligned}\\
    %\tikzset{external/force remake}
    \Rightarrow n\!\left(\gBoxBox[all=gluon,scale=0.8,eA=quark,eB=aquark,
      eLA=$1$,eLB=$2$,eLC=$3$,eLD=$4$,iB=quark,iC=quark,iD=quark,
      iE=quark,iF=quark,iLD=$\ell_2\!\downarrow$]{}\right)
    &= -s\trm(4\ell_231)\kT_{(41)(42)} -s\trp(4\ell_231)\kT_{(31)(32)}
  \end{align*}
\end{frame}


%% \begin{frame}
%%   \frametitle{$\cN=2$ SQCD: Results}
%%   Our representations for the four-point two-loop with external vectors and matter (and mixed) conform to several or all of these properties:
%%   \begin{center}
%%   \begin{tikzpicture}[
%%       mindmap,
%%       every node/.style={
%%         concept,
%%         execute at begin node=\hskip0pt,
%%         inner sep=0cm,
%%         minimum size=0
%%       },
%%       root concept/.append style={
%%         concept color=black, fill=white, line width=1ex, text=black,
%%         text width=2cm,
%%         inner sep=0cm,
%%         minimum size=0
%%       },
%%       text=white,
%%       level 1/.append style={level distance=3cm,sibling angle=90},
%%       level 2/.append style={level distance=2.5cm,sibling angle=45,minimum size=0,inner sep=0},
%%       level 3/.append style={level distance=2cm,sibling angle=45,minimum size=0,inner sep=0}
%%     ]
%%     \node [root concept] {$\cN=2$ SQCD at two-loops}
%%     %child [grow=-150,concept color=red!70!black] { node {SUGRA}
%%     %  child[grow=-135] { node {UV}}
%%     %  child[grow=-90] { node {enhanced cancellations}}
%%     %  child[grow=135] { node {axion \& dilaton}
%%     %    child[grow=0,concept color=blue!50!red] { node {matter ``ghosts''}}
%%     %  }
%%     %}
%%     child [grow=45,concept color=orange] { node {color-dual} }
%%     child [grow=135, concept color=red] { node {SUSY decomposition} }
%%     child [grow=180, concept color=purple] { node {two-term id.} }
%%     child [grow=-135, concept color=blue] { node {matter reversal} }
%%     child [grow=0,concept color=green!50!blue] { node {local} }
%%     child [grow=-45, concept color=green!60!black] { node {manifest CPT} }    
%%     ;
%%   \end{tikzpicture}
%%   \end{center}
%% \end{frame}

\begin{frame}
  \frametitle{$\cN=2$ SCQD: Example results and their properties II}
  The above numerators
    \begin{itemize}
    \item are directly read off from cut
    \item have regulated soft \& collinear limit for hyper lines
    \item have all previously discussed desirable properties: two-term identity, SUSY decomposition, matter-reversal symmetry, manifest CPT
    \end{itemize}
    \vspace{1cm}
    Slightly more difficult:
    \begin{equation*}
      n\left(\gBoxBox[scale=0.7,all=gluon,eLA=$1$,eLB=$2$,eLC=$3$,eLD=$4$,eA=quark,eB=quark,eC=aquark,eD=aquark,iB=aquark,iC=aquark,iE=quark,iF=quark,iLA=$\downarrow\!\ell_1$,iLD=$\ell_2\!\downarrow$]{}\right)
    = -s^2\left[(\ell_1+p_1)\cdot(\ell_1-p_2) + (\ell_2+p_4)\cdot(\ell_2-p_3)\right]\kT_{(12)(34)}
    \end{equation*}
    \begin{itemize}
    \item Cuts do not agree, but Ansatz is reduced to 3 terms (from initially $\sim 300$)
    \item regulates soft \& collinear limit for hyper lines
    \end{itemize}    
\end{frame}

\begin{frame}
  \frametitle{What's next?}
  \begin{itemize}
  \item UV properties of all obtainable theories
  \item Three loops
    \begin{itemize}
    \item color-dual representation: $\cN=2\otimes\cN=2$
    \item vs. local representation: study SQCD IR properties
    \item Iterated multi-particle cuts
    \end{itemize}
  \item Less supersymmetry
    \begin{itemize}
    \item Iterated (local) cut formulae also exist for $\cN=0,1$
    \item Use rung-rule techniques to compute both color-dual and local representation
    \end{itemize}
  \item Challenges:
    \begin{itemize}
    \item Systematize off-shell continuation: Can we make all cuts to agree? Minimal set of terms for an Ansatz?
    \item Color-kinematics duality vs. locality
    \end{itemize}
  \item Simplified gravity cuts from the double copy
  \item External matter in SQCD and SUGRA
  \end{itemize}
\end{frame}

\end{document}
